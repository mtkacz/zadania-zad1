# -*- encoding: utf-8 -*-

import socket

# Ustawienie licznika na zero
z = 0

# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET,
                              socket.SOCK_STREAM,
                              socket.IPPROTO_IP)
#server_socket
#<socket._socketobject object at 0xb745a844>

# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31000)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

while True:
    # Czekanie na połączenie
    connection, client_address = server_socket.accept()

    # Podbicie licznika
    z=z+1

    try:
        # Wysłanie wartości licznika do klienta
        connection.sendall(str(z))
        pass

    finally:
        # Zamknięcie połączenia
        connection.close()
        pass
