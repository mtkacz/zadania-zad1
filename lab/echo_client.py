# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET,
                              socket.SOCK_STREAM,
                              socket.IPPROTO_IP)

# Połączenie z gniazdem nasłuchującego serwera
server_address = ('194.29.175.240', 31000)  # TODO: zmienić port!
server_socket.connect(server_address)

try:
    # Wysłanie danych
    message = u'To jest wiadomość, która zostanie zwrócona.'.encode('utf-8')
    server_socket.sendall(message)

    # Wypisanie odpowiedzi
    print server_socket.recv(16)

finally:
    # Zamknięcie połączenia
    server_socket.close()
    pass