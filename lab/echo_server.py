# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET,
                              socket.SOCK_STREAM,
                              socket.IPPROTO_IP)

# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31000)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

while True:
    # Czekanie na połączenie
    connection, client_address = server_socket.accept()

    try:
        # Odebranie danych i odesłanie ich spowrotem
        respons = connection.recv(16)
        connection.sendall(str(respons))
        pass

    finally:
        # Zamknięcie połączenia
        connection.close()
        pass
